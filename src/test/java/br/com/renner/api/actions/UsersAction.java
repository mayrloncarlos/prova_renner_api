package br.com.renner.api.actions;

import br.com.renner.api.core.BaseAction;
import org.json.simple.JSONObject;

public class UsersAction extends BaseAction {

    protected ThreadLocal<String> body = new ThreadLocal<>();
    protected String idUser;

    protected String name;
    protected String job;
    protected Boolean listarUsers = false;
    protected Boolean consultarUsers = false;
    protected Boolean cadastrarUser = false;
    protected Boolean atualizarUser = false;

    public void montarOBodyDaRequisicaoParaCadastrarUmUsuario(){
        JSONObject json = new JSONObject();
        json.put("name", name);
        json.put("job", job);
        body.set(json.toString());
    }

    public void montarOBodyDaRequisicaoParaAtualizarUmUsuario(){
        JSONObject json = new JSONObject();
        json.put("name", name);
        json.put("job", job);
        body.set(json.toString());
    }
}
