package br.com.renner.api.runners;

import br.com.renner.api.core.BaseTestCase;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;

@CucumberOptions(tags = {"@AllCenarios"},
        features = "src/test/java/br/com/renner/api/features/users.feature",
        glue = {"br.com.renner.api.steps.users"},
        snippets = SnippetType.CAMELCASE)
public class UsersRunner extends BaseTestCase {
}
